* **Simulation scene:** contains a plane and a turtlebot model 
> The turtlebot model contains 3 scripts:
> 1. *Simple_RBC.cs*: is the controller scripts that manages the communication with ROS
> 2. *CmdVel_Controller.cs*: is used for controlling the movement of the robot
> 3. *Laser_Scanner.cs*: is the script for lidar (it is attached under `Turtlebot3Burger/Meshes/Burger_laser/laser_0`)
>  
>  (They are all well commented and pretty easy to understan.)

* **To start the simulation** you need: 

1. Open *Simple_RBC.cs* and find `void Start()` method, change the IP address in `ros = new ROSBridgeWebSocketConnection("ws://192.168.137.141", 9090);` to where ever you are running ROS
> This might not be so straight forward if you are not running ROS and Unity in 2 different devices. If you use a virtual machine to run Linux and ROS on Windows, you may need to do some research to learn how to get the correct address

2. Run *rosbridge_websocket* on ROS: `roslaunch rosbridge_server rosbridge_websocket.launch` and check the terminal logs, if it is already connected you should see
>  See *http://wiki.ros.org/rosbridge_suite* for more details

3. Click **Play** in Unity 

4. Now you should be able to control the robot by run: `roslaunch turtlebot3_teleop turtlebot3_teleop_key.launch` on ROS

5. To Visualize the laser scan points in Rviz (if necessary)
>  1. Write a node to subscribe to the pose message and transform it to tf (to write a tf broadcaster see http://wiki.ros.org/tf/Tutorials)
>  2. Launch robot_state_publisher with the corresponding URDF files (see http://wiki.ros.org/robot_state_publisher
>  * Also see http://emanual.robotis.com/docs/en/platform/turtlebot3/simulation/ and the source code of the tutorial package to check which files are necessary to visualize a turtlebot in rviz) 
>  3. Start Rviz...

*  **Add other Publishers and Subscribers:**
1.  You need to write a new script to define your publisher/subscriber in Unity (check the example under `_Script/Publishers` and `_Script/Subscribers`)
2.  Then add them in `void Start()` method of your controler script.
3.  Then you can publish or subscribe in `void Update ()` method of the controller script or other script (read the example code)
4.  If the types of messages you want to publish have not been defined, you need to write them yourself

> You can find all the currently supported message types under the `ROSBridgeLib` folder 
 