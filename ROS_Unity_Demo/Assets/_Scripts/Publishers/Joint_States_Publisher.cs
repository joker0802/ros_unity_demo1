﻿using SimpleJSON;
using ROSBridgeLib;
using ROSBridgeLib.std_msgs;
using ROSBridgeLib.sensor_msgs;

public class Joint_States_Publisher : ROSBridgePublisher
{
    // The following three functions are important
    public new static string GetMessageTopic()
    {
        return "joint_states";
    }

    public new static string GetMessageType()
    {
        return "sensor_msgs/JointState";
    }

    public static string ToYAMLString(JointStateMsg msg)
    {
        return msg.ToYAMLString();
    }

    public static ROSBridgeMsg ParseMessage(JSONNode msg)
    {
        return new JointStateMsg(msg);
    }
}
