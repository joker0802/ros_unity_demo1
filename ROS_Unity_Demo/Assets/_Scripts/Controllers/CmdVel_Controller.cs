﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CmdVel_Controller : MonoBehaviour
{
    private double linearVelocity = 0;
    private double angularVelocity = 0;
    public Simple_RBC activatedController;

    void Awake()
    {
        linearVelocity = 0;
        angularVelocity = 0;
    }

    // Update is called once per frame
    void Update()
    {
        if (activatedController.GetLinear() != null)
        {
            linearVelocity = activatedController.GetLinear().GetX();
            angularVelocity = -activatedController.GetAngular().GetZ();

            transform.Translate(new Vector3(0, 0, (float)linearVelocity) * Time.deltaTime, Space.Self);
            transform.Rotate(0.0f, (float)angularVelocity * (180 / Mathf.PI) * Time.deltaTime, 0.0f, Space.Self);
        }
    }
}

