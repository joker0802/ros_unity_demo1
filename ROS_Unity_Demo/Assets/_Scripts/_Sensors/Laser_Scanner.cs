﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Laser_Scanner : MonoBehaviour {

    private float[] hits = new float[360];
    private float[] intensities = new float[360];

    void Update () {

        int RaysToShoot = 360; //360 rays per frame
        float angle = 0; //Initial angle
        float theDistance;
        
        for (int i = 0; i < RaysToShoot; i++)
        {
            float x = Mathf.Sin(angle);
            float z = Mathf.Cos(angle);
            angle += 2 * Mathf.PI / RaysToShoot;

            //Define the direction and range of each laser beam 
            Vector3 forward = transform.TransformDirection(new Vector3(x, 0, z)) * 3.6f;

            RaycastHit hit;

            //Debug.DrawRay(transform.position, forward, Color.red); //Showing red rays

            if (Physics.Raycast(transform.position, (forward), out hit))
            {
                theDistance = hit.distance;
                //Debug.Log(theDistance);
                hits[i] = theDistance;
                intensities[i] = 550;
            }
            else
            {
                hits[i] = 3.6f;
                intensities[i] = 550;
            }
        }
    }
    public float[] GetHits() //getter
    {
        return hits;
    }

    public float[] GetIntensity() 
    {
        return intensities;
    }
}
