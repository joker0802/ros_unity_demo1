﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using SimpleJSON;
using ROSBridgeLib.std_msgs;

namespace ROSBridgeLib
{
    namespace geometry_msgs
    {
        public class PoseWithCovarianceStampedMsg : ROSBridgeMsg
        {
            public HeaderMsg _header;
            public PoseWithCovarianceMsg _posewc;

            public PoseWithCovarianceStampedMsg(JSONNode msg)
            {
                _posewc = new PoseWithCovarianceMsg(msg["pose"]);
                _header = new HeaderMsg(msg["header"]);
            }

            public PoseWithCovarianceStampedMsg(HeaderMsg header, PoseWithCovarianceMsg posewc)
            {
                _header = header;
                _posewc = posewc;
            }

            public static string GetMessageType()
            {
                return "geometry_msgs/PoseWithCovarianceStamped";
            }

            public PoseWithCovarianceMsg GetPoseWC()
            {
                return _posewc;
            }

            public HeaderMsg GetHeader()
            {
                return _header;
            }

            public override string ToString()
            {
                return "PoseWithCovarianceStamped [header=" + _header.ToString() + ",  pose=" + _posewc.ToString() + "]";
            }

            public override string ToYAMLString()
            {
                return "{\"header\" : " + _header.ToYAMLString() + ", \"pose\" : " + _posewc.ToYAMLString() + "}";
            }
        }
    }
}
